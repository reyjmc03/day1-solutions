package ph.groundgurus.day1.solutions.exercise5;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Reference:
 * https://www.journaldev.com/12014/opencsv-csvreader-csvwriter-example
 * https://www.mkyong.com/java/apache-poi-reading-and-writing-excel-file-in-java/
 *
 * @author pgutierrez
 */
public class Day1Exercise5 {

    public static void main(String[] args) throws IOException {
        EmployeeService employeeService = new EmployeeService();
        final List<Employee> employees = employeeService.getAll();
        
        MSExcelService msExcelService = new MSExcelService();
        
        String filePath = System.getProperty("user.home") + File.separator + "day1exercise5.xlsx";
        
        // creates the xlsx file in the user home
        msExcelService.toMSExcel(employees, filePath);
    }
}
