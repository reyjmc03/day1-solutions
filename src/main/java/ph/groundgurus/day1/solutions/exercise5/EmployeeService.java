package ph.groundgurus.day1.solutions.exercise5;

import com.opencsv.CSVReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pgutierrez
 */
public class EmployeeService {
    public List<Employee> getAll() throws IOException {
        List<Employee> employees = new ArrayList<>();
        
        final String csvFile = EmployeeService.class.getClassLoader()
                .getResource("day1exercise5.csv").getFile();
        
        try (CSVReader reader = new CSVReader(new FileReader(csvFile), ',')) {    
            // read line by line
            String[] record;
            
            while ((record = reader.readNext()) != null) {
                Employee employee = new Employee();
                employee.setId(record[0]);
                employee.setName(record[1]);
                employee.setAge(record[2]);
                employee.setCountry(record[3]);
                
                employees.add(employee);
            }
        }
        
        return employees;
    }
}
