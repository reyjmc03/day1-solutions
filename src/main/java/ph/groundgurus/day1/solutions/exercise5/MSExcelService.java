package ph.groundgurus.day1.solutions.exercise5;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author pgutierrez
 */
public class MSExcelService {

    public void toMSExcel(List<Employee> employees, String filepath) throws IOException {
        try (XSSFWorkbook workbook = new XSSFWorkbook()) {
            XSSFSheet sheet = workbook.createSheet("Employees");
            System.out.println("Creating the excel file...");
            int rowNum = 0;
            
            for (Employee employee : employees) {
                XSSFRow row = sheet.createRow(rowNum++);
                
                int colNum = 0;
                
                createCell(row, colNum++, employee.getId());
                createCell(row, colNum++, employee.getName());
                createCell(row, colNum++, employee.getAge());
                createCell(row, colNum++, employee.getCountry());
            }
            
            try (FileOutputStream outputStream = new FileOutputStream(filepath)) {
                workbook.write(outputStream);
            }
        }

        System.out.println("Done.");
    }

    private void createCell(XSSFRow row, int colNum, String cellValue) {
        XSSFCell cell = row.createCell(colNum, CellType.STRING);
        cell.setCellValue(cellValue);
    }
}
