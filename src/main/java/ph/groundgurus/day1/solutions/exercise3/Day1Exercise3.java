package ph.groundgurus.day1.solutions.exercise3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Reference: https://www.mkyong.com/java/how-to-determine-a-prime-number-in-java/
 * 
 * @author pgutierrez
 */
public class Day1Exercise3 {

    public static void main(String[] args) {
        List<Integer> someNumbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);

        // get the prime numbers
        List<Integer> primeNumbers = getPrimeNumbers(someNumbers);

        System.out.println(primeNumbers);
    }

    private static List<Integer> getPrimeNumbers(List<Integer> someNumbers) {
        List<Integer> primeNumbers = new ArrayList<>();
        
        for (Integer someNumber : someNumbers) {
            if (isPrime(someNumber)) {
                primeNumbers.add(someNumber);
            }
        }
        
        return primeNumbers;
    }

    private static boolean isPrime(int n) {
        //check if n is a multiple of 2
        if (n % 2 == 0) {
            return false;
        }
        //if not, then just check the odds
        for (int i = 3; i * i <= n; i += 2) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }
}
