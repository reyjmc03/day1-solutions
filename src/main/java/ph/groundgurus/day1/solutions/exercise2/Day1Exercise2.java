package ph.groundgurus.day1.solutions.exercise2;

/**
 *
 * @author pgutierrez
 */
public class Day1Exercise2 {

    public static void main(String[] args) {
        String shape = "rectangle";

        switch (shape) {
            case "rectangle":
                computeRectangle(15, 20);
                break;
            case "square":
                computeSquare(10);
                break;
            case "circle":
                computeCircle(15);
                break;
            default:
                System.err.println("Sorry, that shape is not supported");
        }
    }

    private static void computeRectangle(int length, int width) {
        int area = length * width;
        System.out.println("The area of a rectangle with a length of " + length
                + "and width of " + width + " is " + area);
    }

    private static void computeSquare(int side) {
        int area = side * side;
        System.out.println("The area of a square with the side of " + side
                + " is " + area);
    }

    private static void computeCircle(double radius) {
        double area = Math.PI * (radius * radius);
        System.out.println("The area of a square with a radius of " + radius
                + " is " + area);
    }
}
