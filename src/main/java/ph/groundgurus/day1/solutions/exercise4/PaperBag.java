package ph.groundgurus.day1.solutions.exercise4;

import java.util.ArrayList;

/**
 * For testing this kindly see GroceryStoreTest.java
 * 
 * Created by pgutierrez
 */
public class PaperBag extends Bag {

    public void removeItems() {
        groceries = new ArrayList<>();
    }
}
