package ph.groundgurus.day1.solutions.exercise4;

import java.util.ArrayList;
import java.util.List;

/**
 * For testing this kindly see GroceryStoreTest.java
 * 
 * Created by pgutierrez
 */
public class Bag extends GroceryItem {
    protected List<GroceryItem> groceries = new ArrayList<>();

    public void addItems(List<GroceryItem> groceries) {
        this.groceries = groceries;
    }
}
