package ph.groundgurus.day1.solutions.exercise4;

/**
 * For testing this kindly see GroceryStoreTest.java
 * 
 * Created by pgutierrez
 */
public class Customer {
    private String name;
    private double money;

    public Customer(String name, double money) {
        this.name = name;
        this.money = money;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public void carryItem(GroceryItem groceryItem) {
        // carry the grocery item
    }
}
