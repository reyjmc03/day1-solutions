package ph.groundgurus.day1.solutions.exercise4;

import java.util.*;

/**
 * For testing this kindly see GroceryStoreTest.java
 * 
 * For testing this kindly see GroceryStoreTest.java
 * 
 * Created by pgutierrez
 */
public class GroceryStore {
    private List<Customer> customers = new ArrayList<>();
    private List<Cashier> cashiersAvailable = new ArrayList<>();
    private Map<String, Queue<GroceryItem>> storeItems = new HashMap<>();
    private double profit;

    public GroceryStore() {
        Queue<GroceryItem> eggs = new PriorityQueue<>();
        eggs.add(new GroceryItem("Egg", 5));
        eggs.add(new GroceryItem("Egg", 5));
        eggs.add(new GroceryItem("Egg", 5));
        storeItems.put("Egg", eggs);

        Queue<GroceryItem> coconuts = new PriorityQueue<>();
        coconuts.add(new GroceryItem("Coconut", 15));
        storeItems.put("Coconut", coconuts);

        Queue<GroceryItem> bagsOfCatFood = new PriorityQueue<>();
        bagsOfCatFood.add(new GroceryItem("Bag of Cat Food", 150));
        bagsOfCatFood.add(new GroceryItem("Bag of Cat Food", 150));
        storeItems.put("Bag of Cat Food", bagsOfCatFood);
    }

    public GroceryItem getGroceryItem(String item) {
        Queue<GroceryItem> items = storeItems.get(item);
        return items.remove();
    }

    public void addCustomer(Customer customer) {
        customers.add(customer);
    }

    public void goToWork(Cashier cashier) {
        cashiersAvailable.add(cashier);
    }

    public void addProfit(double profit) {
        this.profit += profit;
    }

    public double getProfit() {
        return profit;
    }
}
