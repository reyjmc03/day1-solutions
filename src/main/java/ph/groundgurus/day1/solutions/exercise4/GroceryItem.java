package ph.groundgurus.day1.solutions.exercise4;

/**
 * For testing this kindly see GroceryStoreTest.java
 * 
 * Created by pgutierrez
 */
public class GroceryItem implements Comparable<GroceryItem> {
    private String name;
    private double price;

    public GroceryItem() {
    }

    public GroceryItem(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public int compareTo(GroceryItem otherItem) {
        return name.compareTo(otherItem.name);
    }
}
