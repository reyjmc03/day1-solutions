package ph.groundgurus.day1.solutions.exercise4;

import java.util.Arrays;
import java.util.List;

/**
 * For testing this kindly see GroceryStoreTest.java
 * 
 * Created by pgutierrez
 */
public class Cashier {
    private String name;

    public Cashier(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void scan(GroceryStore groceryStore, GroceryItem grocery, Customer customer) {
        scan(groceryStore, Arrays.asList(grocery), customer);
    }

    public void scan(GroceryStore groceryStore, List<GroceryItem> groceries, Customer customer) {
        double cost = 0.0;

        // compute the total cost
        for (GroceryItem groceryItem : groceries) {
            cost += groceryItem.getPrice();
        }

        // get the payment from the customer
        double change = customer.getMoney() - cost;

        // give the change
        customer.setMoney(change);

        // add the profit to the grocery store
        groceryStore.addProfit(cost);
    }
}
