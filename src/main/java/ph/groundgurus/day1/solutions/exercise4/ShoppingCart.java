package ph.groundgurus.day1.solutions.exercise4;

import java.util.ArrayList;
import java.util.List;

/**
 * For testing this kindly see GroceryStoreTest.java
 * 
 * Created by pgutierrez
 */
public class ShoppingCart {
    private List<GroceryItem> groceries = new ArrayList<>();

    public void addItem(GroceryItem groceryItem) {
        groceries.add(groceryItem);
    }

    public List<GroceryItem> getGroceries() {
        return groceries;
    }
}
