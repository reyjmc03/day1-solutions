package ph.groundgurus.day1.solutions.exercise1;

/**
 *
 * @author pgutierrez
 */
public class Day1Exercise1 {

    public static void main(String[] args) {
        int someNumber = 5;

        for (int i = 1; i <= 10; i++) {
            int product = someNumber * i;
            System.out.println(someNumber + " * " + i + " = " + product);
        }
    }
}
