package ph.groundgurus.day1.solutions.exercise4;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by pgutierrez
 */
public class GroceryStoreTest {

    @Test
    public void testGrocery() throws Exception {
        // old lady has 500 pesos
        Customer oldLady = new Customer("Old Lady", 500);

        // grocery store
        GroceryStore pureGold = new GroceryStore();

        // cashier goes to work
        Cashier jessy = new Cashier("Jessy");
        pureGold.goToWork(jessy);

        // old lady comes in the store
        pureGold.addCustomer(oldLady);

        // old lady gets a shopping cart and gets her groceries
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addItem(pureGold.getGroceryItem("Egg"));
        shoppingCart.addItem(pureGold.getGroceryItem("Egg"));
        shoppingCart.addItem(pureGold.getGroceryItem("Egg"));
        shoppingCart.addItem(pureGold.getGroceryItem("Coconut"));
        shoppingCart.addItem(pureGold.getGroceryItem("Bag of Cat Food"));
        shoppingCart.addItem(pureGold.getGroceryItem("Bag of Cat Food"));

        // scan the grocery items and get payment from old lady
        List<GroceryItem> groceries = shoppingCart.getGroceries();
        jessy.scan(pureGold, groceries, oldLady);

        // paper bags
        PaperBag paperBag = new PaperBag();

        // cashier adds the items in the paper bags
        paperBag.addItems(groceries);

        // old lady decides to buy an eco bag
        EcoBag ecoBag = new EcoBag();
        ecoBag.addItems(groceries);
        paperBag.removeItems();

        // cashier charges for the eco bag
        jessy.scan(pureGold, ecoBag, oldLady);

        // old lady carries the eco bag
        oldLady.carryItem(ecoBag);

        // check the remaining money of the old lady
        assertEquals(170.0, oldLady.getMoney(), 0.0);

        // check old lady's remaining money
        assertEquals(330.0, pureGold.getProfit(), 0.0);
    }
}
